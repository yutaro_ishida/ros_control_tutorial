#ifndef ROBOT_H_
#define ROBOT_H_

#include <ros/ros.h>

#include <hardware_interface/robot_hw.h>

// Controllerと現在値を交換するためのインタフェース（位置，速度，力）
#include <hardware_interface/joint_state_interface.h>
// Controllerと指令値を交換するためのインタフェース（速度）
#include <hardware_interface/joint_command_interface.h>

#include <transmission_interface/simple_transmission.h>
#include <transmission_interface/transmission_interface.h>

class Robot : public hardware_interface::RobotHW {
public:
  Robot(void);
  ~Robot(void) {};
  
  ros::NodeHandle nh_;
    
  void read(
    ros::Time,
    ros::Duration);
  void write(
    ros::Time,
    ros::Duration);
  
private:
  // Controllerと現在値を交換するためのインタフェース（位置，速度，力）
  hardware_interface::JointStateInterface joint_state_interface_;
  // Controllerと指令値を交換するためのインタフェース（速度）
  hardware_interface::VelocityJointInterface joint_command_interface_;

  // 関節軸とアクチュエータ軸の値を相互変換するための係数
  transmission_interface::SimpleTransmission transmission_;
  // アクチュエータ軸の現在値から関節軸の現在値に変換するインタフェース（位置，速度，力）
  transmission_interface::ActuatorToJointStateInterface actuator_to_joint_state_interface_;
  // 関節軸の指令値からアクチュエータ軸の指令値に変換するインタフェース（速度）
  transmission_interface::JointToActuatorVelocityInterface joint_to_actuator_position_interface_;

  // 関節軸
  // 指令値
  double joint_command_velocity_;  // 速度
  // 現在値（無効な項目があっても要求されるので3項目分用意しておく）
  double joint_present_position_;  // 位置
  double joint_present_velocity_;  // 速度
  double joint_present_effort_;  // 力
  // アクチュエータ軸
  // 指令値
  double actuator_command_velocity_;  // 速度
  // 現在値（無効な項目があっても要求されるので3項目分用意しておく）
  double actuator_present_position_;  // 位置
  double actuator_present_velocity_;  // 速度
  double actuator_present_effort_;  // 力
};

#endif  // ROBOT_H
