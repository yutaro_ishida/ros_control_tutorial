#include "robot.hpp"
#include <controller_manager/controller_manager.h>

int main(
  int argc,
  char *argv[]
) {
  ros::init(argc, argv, "robot_node");

  ros::Duration period(1.0 / 50.0);  // 制御周期 [s]

  Robot robot;
  controller_manager::ControllerManager cm(&robot);

  ros::Rate rate(1.0 / period.toSec());

  // ros::spin()およびros::spinOnce()の代わり
  // 別スレッドを立ててコールバックを監視する
  ros::AsyncSpinner spinner(1);  // スレッド数
  spinner.start();
  
  while (ros::ok()) {
    ros::Time now = ros::Time::now();
    
    // 1制御周期で
    robot.read(now, period);  // アクチュエータから値を読む
    cm.update(now, period);  // 次の制御量を決める
    robot.write(now, period);  // アクチュエータに値を書く

    rate.sleep();
  }

  spinner.stop();
  
  return 0;
}
