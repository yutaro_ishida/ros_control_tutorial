#include "robot.hpp"

Robot::Robot(
  void
) :
transmission_(10.0, 0.0) {
  // transmission_interface::SimpleTransmissionのコンストラクタ
  // 関節軸とアクチュエータ軸の値を相互変換するための係数
  // 引数1: 減速比 関節軸:アクチュエータ軸 1.0:10.0
  // 引数2: オフセット [rad]

  // 値の初期化
  // 関節軸
  // 指令値
  joint_command_position_ = 0.0;  // 位置
  // 現在値
  joint_present_position_ = 0.0;  // 位置
  joint_present_velocity_ = 0.0;  // 速度
  joint_present_effort_ = 0.0;  // 力
  // アクチュエータ軸
  // 指令値
  actuator_command_position_ = 0.0;  // 位置
  // 現在値
  actuator_present_position_ = 0.0;  // 位置
  actuator_present_velocity_ = 0.0;  // 速度
  actuator_present_effort_ = 0.0;  // 力

  // Controllerと現在値を交換するためのインタフェースにハンドルを登録する（位置，速度，力）
  // 無効な項目があっても3項目要求される
  hardware_interface::JointStateHandle joint_state_handle_(
    "base2arm_joint",
    &joint_present_position_,
    &joint_present_velocity_,
    &joint_present_effort_);
  joint_state_interface_.registerHandle(joint_state_handle_);
  registerInterface(&joint_state_interface_);

  // Controllerと指令値を交換するためのインタフェースにハンドルを登録する（位置）
  hardware_interface::JointHandle joint_command_handle_(
    joint_state_handle_,
    &joint_command_position_);
  joint_command_interface_.registerHandle(joint_command_handle_);
  registerInterface(&joint_command_interface_);  

  // アクチュエータ軸の現在値から関節軸の現在値に変換するインタフェースにハンドルを登録する（位置，速度，力）
  // read()，アクチュエータから値を読む時に使用する
  // 無効な項目があっても3項目要求される
  transmission_interface::ActuatorData actuator_present_data;
  transmission_interface::JointData joint_present_data;

  actuator_present_data.position.push_back(&actuator_present_position_); 
  actuator_present_data.velocity.push_back(&actuator_present_velocity_);
  actuator_present_data.effort.push_back(&actuator_present_effort_);
  joint_present_data.position.push_back(&joint_present_position_);
  joint_present_data.velocity.push_back(&joint_present_velocity_);
  joint_present_data.effort.push_back(&joint_present_effort_);

  actuator_to_joint_state_interface_.registerHandle(
    transmission_interface::ActuatorToJointStateHandle(
      "actuator_to_joint_state",
      &transmission_,
      actuator_present_data,
      joint_present_data));
  // 関節軸の指令値からアクチュエータ軸の指令値に変換するインタフェースにハンドルを登録する（位置）
  // write()，アクチュエータに値を書く時に使用する
  transmission_interface::ActuatorData actuator_command_data;
  transmission_interface::JointData joint_command_data;

  actuator_command_data.position.push_back(&actuator_command_position_);
  joint_command_data.position.push_back(&joint_command_position_);

  joint_to_actuator_position_interface_.registerHandle(
    transmission_interface::JointToActuatorPositionHandle(
      "joint_to_actuator_position",
      &transmission_,
      actuator_command_data,
      joint_command_data));
}

void Robot::read(
  ros::Time time,
  ros::Duration period
) {
  // ここでアクチュエータから値を読む
  actuator_present_position_ = actuator_command_position_;  // このサンプルでは指令値を現在値としてオウム返しする
  // actuator_present_velocity_;  // 速度が読めないアクチュエータを想定し何もしない
  // actuator_present_effort_;  // 力が読めないアクチュエータを想定し何もしない
 
  // アクチュエータ軸の現在値（変数actuator_present_hoge_）から関節軸の現在値（変数joint_present_hoge_）へ変換する
  actuator_to_joint_state_interface_.propagate(); 

  ROS_INFO("read():");
  printf(
    "joint_present_position_=%lf actuator_present_position_=%lf\n",
    joint_present_position_,
    actuator_present_position_);
}

void Robot::write(
  ros::Time time,
  ros::Duration period
) {
  // 関節軸の指令値（変数joint_command_hoge_）からアクチュエータ軸の指令値（変数actuator_command_hoge_）へ変換する
  joint_to_actuator_position_interface_.propagate();

  // ここでアクチュエータに値を書く
  // actuator_command_posiiton_;

  ROS_INFO("write():");
  printf(
    "joint_command_position_=%lf actuator_command_position_=%lf\n",
    joint_command_position_,
    actuator_command_position_);
}
