# これは？
ROS Control初学者が理解できるリポジトリ

# とりあえず
```
位置制御ロボットのサンプル
[新しい端末]
$ roslaunch ros_control_tutorial joint_position_controller.launch
[新しい端末]
$ rostopic pub /robot/base2arm_joint_controller/command std_msgs/Float64 "data: 1.0"

速度制御ロボットのサンプル
[新しい端末]
$ roslaunch ros_control_tutorial joint_velocity_controller.launch
[新しい端末]
$ rostopic pub /robot/base2arm_joint_controller/command std_msgs/Float64 "data: 1.0"

軌道制御ロボットのサンプル
[新しい端末]
$ roslaunch ros_control_tutorial joint_trajectory_controller.launch
[新しい端末]
$ rostopic pub /robot/base2arm_joint_controller/command trajectory_msgs/JointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
- 'base2arm_joint'
points:
- positions: [1.0]
  velocities: []
  accelerations: []
  effort: []
  time_from_start: {secs: 10, nsecs: 0}" 
```